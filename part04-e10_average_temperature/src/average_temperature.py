#!/usr/bin/env python3

import pandas as pd

def average_temperature():

    df = pd.read_csv('src/kumpula-weather-2017.csv', header=0)

    return df[df.m == 7]['Air temperature (degC)'].mean()

 
def main():

    temp = average_temperature()
    print(f'Average temperature in July: {temp:.1f}')

if __name__ == "__main__":
    main()
