#!/usr/bin/env python3

import pandas as pd

def suicide_fractions():

    suicide = pd.read_csv('src/who_suicide_statistics.csv')
    suicide['prop'] = suicide.suicides_no / suicide.population
    
    return suicide.groupby('country').mean()['prop']

def main():

    suicide_fractions()

if __name__ == "__main__":
    main()
