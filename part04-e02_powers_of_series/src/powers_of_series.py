#!/usr/bin/env python3

import pandas as pd


def powers_of_series(s, k):

    powers = {}

    for i in range(1, k + 1):
        powers[i] = s ** i

    return pd.DataFrame(powers)

    
def main():

    s = pd.Series([1, 2, 3, 4], index=list('abcd'))
    powers_of_series(s, 3)    

if __name__ == "__main__":
    main()
