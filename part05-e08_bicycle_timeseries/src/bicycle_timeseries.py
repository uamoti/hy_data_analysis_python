#!/usr/bin/env python3

import pandas as pd
import re


def bicycle_timeseries():

    month = {
        'tammi': '01',
        'helmi': '02',
        'maalis': '03',
        'huhti': '04',
        'touko': '05',
        'kesä': '06',
        'heinä': '07',
        'elo': '08',
        'syys': '09',
        'loka': '10',
        'marras': '11',
        'joulu': '12'
    }
    pattern = '^[a-z][a-z] (\d+) (\w+) ([1-2][0-9][0-9][0-9]) ([0-2][0-9]:[0-5][0-9])'
    bike = pd.read_csv('src/Helsingin_pyorailijamaarat.csv', sep=';', header=0)
    bike.dropna(subset=['Päivämäärä'], inplace=True)
    bike.drop('Unnamed: 21', axis=1, inplace=True)
    match = bike.Päivämäärä.apply(lambda x: re.match(pattern, x).groups())
    match = match.apply(lambda x: (x[0], month[x[1]], x[2], x[3]))
    match = match.apply(' '.join)
    bike.Päivämäärä = pd.to_datetime(match)

    return bike.set_index('Päivämäärä')


def main():

    bicycle_timeseries()

if __name__ == "__main__":
    main()
