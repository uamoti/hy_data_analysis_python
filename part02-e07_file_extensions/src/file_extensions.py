#!/usr/bin/env python3

import sys
import re

def file_extensions(filename):

    no_ext = []
    extensions = {}

    for line in open(filename):
        line = line.strip()
        if '.' not in line:
            no_ext.append(line)
        else:
            ext_pattern = r'\.(\w+)$'
            ext = re.findall(ext_pattern, line)[0]
            if ext in extensions:
                extensions[ext].append(line)
            else:
                extensions[ext] = [line]

    return no_ext, extensions


def main():

    noext, exts = file_extensions('src/filenames.txt')
    print(f'{len(noext)} files with no extension')
    for e in exts.keys():
        print(f'e {len(exts[e])}')

if __name__ == "__main__":
    main()
