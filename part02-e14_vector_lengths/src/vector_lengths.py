#!/usr/bin/env python3

import numpy as np
#import scipy.linalg

def vector_lengths(x):

    if len(x.shape) == 1:
        return np.array(np.sqrt(np.sum(x ** 2)))
    else:
        return np.sqrt(np.sum(x ** 2, axis=1))

 
def main():

    vector_lengths(np.random.randint(0, 10000, (100, 20)))

if __name__ == "__main__":
    main()
