#!/usr/bin/env python3

def reverse_dictionary(d):

    reverse = {}

    for k in d.keys():
        finn = d[k]
        for f in finn:
            if f in reverse:
                reverse[f].append(k)
            else:
                reverse[f] = [k]

    return reverse

def main():
    pass

if __name__ == "__main__":
    main()
