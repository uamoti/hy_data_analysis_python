#!/usr/bin/env python3

def sum_equation(L):

    if len(L) == 0:
        return '0 = 0'
    else:
        total = sum(L)
        eq = ' + '.join([str(n) for n in L])
        eq += ' = ' + str(total)
        return eq

def main():
    pass

if __name__ == "__main__":
    main()
