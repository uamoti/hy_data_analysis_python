#!/usr/bin/env python3

import re

def red_green_blue(filename="src/rgb.txt"):

    def format_output(l):

        r, g, b = l[0].split()

        return '\t'.join([r, g, b, l[1]])

    
    rgb = [l.strip() for l in open('src/rgb.txt').readlines()[1:]]
    pattern = re.compile(r'(\d+\s+\d+\s+\d+)\s+(\w+\s?\w*)')
    result = [pattern.findall(l)[0] for l in rgb]
    result = [format_output(l) for l in result]

    return result


def main():

    red_green_blue()

if __name__ == "__main__":
    main()
