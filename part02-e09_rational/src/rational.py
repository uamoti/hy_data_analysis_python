#!/usr/bin/env python3

class Rational(object):

    def __init__(self, dn, dv):

        self.denominator = dn
        self.divisor = dv

    def __add__(self, other):

        new_div = self.divisor * other.divisor
        denom1 = (new_div / self.divisor) * self.denominator
        denom2 = (new_div / other.divisor) * other.denominator

        return Rational((denom1 + denom2), new_div)

    def __sub__(self, other):

        new_div = self.divisor * other.divisor
        denom1 = (new_div / self.divisor) * self.denominator
        denom2 = (new_div / other.divisor) * other.denominator

        return Rational((denom1 - denom2), new_div)

    def __mul__(self, other):

        new_denom = self.denominator * other.denominator
        new_div = self.divisor * other.divisor

        return Rational(new_denom, new_div)

    def __truediv__(self, other):

        new_denom = self.denominator * other.divisor
        new_div = self.divisor * other.denominator

        return Rational(new_denom, new_div)

    def __eq__(self, other):

        return self.denominator == other.denominator and self.divisor == other.divisor

    
    def __lt__(self, other):

        return self.denominator < other.denominator and self.divisor > other.divisor

    
    def __gt__(self, other):

        return self.denominator > other.denominator and self.divisor < other.divisor

    def __str__(self):

        return '{}/{}'.format(self.denominator, self.divisor)

    

def main():
    r1=Rational(1,4)
    r2=Rational(2,3)
    print(r1)
    print(r2)
    print(r1*r2)
    print(r1/r2)
    print(r1+r2)
    print(r1-r2)
    print(Rational(1,2) == Rational(2,4))
    print(Rational(1,2) > Rational(2,4))
    print(Rational(1,2) < Rational(2,4))

if __name__ == "__main__":
    main()
