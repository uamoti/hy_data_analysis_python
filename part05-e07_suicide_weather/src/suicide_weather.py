#!/usr/bin/env python3

import pandas as pd


def suicide_fractions():

    suicide = pd.read_csv('src/who_suicide_statistics.csv')
    suicide['prop'] = suicide.suicides_no / suicide.population
    
    return suicide.groupby('country').mean()['prop']

def suicide_weather():

    props = suicide_fractions()
    temp = pd.read_html('src/List_of_countries_by_average_yearly_temperature.html', header=0, index_col=0)[0]
    temp.index.name = 'country'
    temp.columns = pd.Index(['temperature'])
    temp.temperature = temp.temperature.apply(lambda x: float(x.replace('\u2212', '-')))
    merge = pd.merge(props, temp, left_index=True, right_index=True)
    spearman = props.corr(temp.temperature, method='spearman')
    
    return (props.shape[0], temp.shape[0], merge.shape[0], spearman)

def main():

    s_rows, t_rows, c_rows, corr = suicide_weather()
    print(f'Suicide DataFrame has {s_rows} rows')
    print(f'Temperature DataFrame has {t_rows} rows')
    print(f'Common DataFrame has {c_rows} rows')
    print(f'Spearman correlation: {corr}')

if __name__ == "__main__":
    main()
