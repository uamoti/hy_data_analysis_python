#!/usr/bin/env python3

import pandas as pd

def cities():

    data = {
        'Population': {
            'Helsinki': 643272,
            'Espoo': 279044,
            'Tampere': 231853,
            'Vantaa': 223027,
            'Oulu': 201810
        },
        'Total area': {
            'Helsinki': 715.48,
            'Espoo': 528.03 ,
            'Tampere': 689.59,
            'Vantaa': 240.35,
            'Oulu': 3817.52
        }
    }

    return pd.DataFrame(data)
    
def main():

    cities()
    
if __name__ == "__main__":
    main()
