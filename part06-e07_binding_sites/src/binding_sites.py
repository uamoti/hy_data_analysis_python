#!/usr/bin/env python3

import pandas as pd
import numpy as np
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import accuracy_score
from sklearn.metrics import pairwise_distances
from matplotlib import pyplot as plt
import seaborn as sns
import scipy as sp
import scipy.cluster.hierarchy as hc

sns.set(color_codes=True)

def find_permutation(n_clusters, real_labels, labels):

    permutation=[]
    for i in range(n_clusters):
        idx = labels == i
        new_label=sp.stats.mode(real_labels[idx])[0][0]  # Choose the most common label among data points in the cluster
        permutation.append(new_label)

    return permutation

def toint(nucl):

    conversion = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
    
    return conversion[nucl]


def to_int(seq):

    conversion = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
    
    return [conversion[nucleotide] for nucleotide in seq]

def get_features_and_labels(filename):

    df = pd.read_csv(filename, header=0, sep='\t')
    X = np.matrix(df.X.apply(to_int).to_list())
    
    return X, df.y.to_numpy()

def plot(distances, method='average', affinity='euclidean'):

    mylinkage = hc.linkage(sp.spatial.distance.squareform(distances), method=method)
    g = sns.clustermap(distances, row_linkage=mylinkage, col_linkage=mylinkage )
    g.fig.suptitle(f"Hierarchical clustering using {method} linkage and {affinity} affinity")
    plt.show()

def cluster_euclidean(filename):

    X, y = get_features_and_labels(filename)
    clust = AgglomerativeClustering(affinity='euclidean', linkage='average').fit(X)
    perm = find_permutation(2, y, clust.labels_)
    new_labels = [perm[label] for label in clust.labels_]
    
    return accuracy_score(y, new_labels)


def cluster_hamming(filename):

    X, y = get_features_and_labels(filename)
    dist = pairwise_distances(X, metric='hamming')
    clust = AgglomerativeClustering(linkage='average', affinity='precomputed').fit_predict(dist)
    perm = find_permutation(2, y, clust)
    new_labels = [perm[label] for label in clust]
    
    return accuracy_score(y, new_labels)


def main():
    print("Accuracy score with Euclidean affinity is", cluster_euclidean("src/data.seq"))
    print("Accuracy score with Hamming affinity is", cluster_hamming("src/data.seq"))

if __name__ == "__main__":
    main()
