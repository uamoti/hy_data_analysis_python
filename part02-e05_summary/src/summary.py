#!/usr/bin/env python3

import sys
import math

def summary(filename='example.txt'):
    
    if filename == '':
        sys.exit(1)

    nums = []

    for line in open(filename):
        try:
            nums.append(float(line))
        except ValueError:
            pass

    total = sum(nums)
    if total ==	0:
        avg = 0
        sd = math.sqrt(sum([x ** 2 for x in nums]) / (len(nums) - 1))
    else:
        avg = total / len(nums)
        sd = math.sqrt(sum([(x - avg) ** 2 for x in nums]) / (len(nums) - 1))

    return total, avg, sd


def main():

    for filename in sys.argv[1:]:
        t, avg, sd = summary(filename)
        print(f'File: {filename} Sum: {t:.6f} Average: {avg:.6f} Stddev: {sd:.6f}')
        

if __name__ == "__main__":
    main()
