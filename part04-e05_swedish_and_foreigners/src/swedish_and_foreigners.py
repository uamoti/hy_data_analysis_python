#!/usr/bin/env python3

import pandas as pd

def swedish_and_foreigners():

    df = pd.read_csv('src/municipal.tsv', header=0, index_col=0, sep='\t')
    df = df['Akaa':'Äänekoski']
    cols = pd.Index([
        'Population',
        'Share of Swedish-speakers of the population, %',
        'Share of foreign citizens of the population, %',
    ])

    df = df[(df[cols[1]] > 5) & (df[cols[2]] > 5)]

    return df[cols]

def main():

    swedish_and_foreigners()

if __name__ == "__main__":
    main()
