#!/usr/bin/env python3


def main():
    # Enter you solution here
    land = input("What country are you from? ")
    print(f"I have heard that {land} is a beautiful country.")


if __name__ == "__main__":
    main()
