#!/usr/bin/env python3

import pandas as pd
import numpy as np
import scipy as sp
from sklearn.cluster import DBSCAN
from sklearn.metrics import accuracy_score


def find_permutation(n_clusters, real_labels, labels):

    permutation=[]
    mask = np.ma.masked_values(labels, -1)
    
    for i in range(n_clusters):
        idx = labels == i
        # Choose the most common label among data points in the cluster
        new_label=sp.stats.mode(real_labels[idx])[0][0]
        permutation.append(new_label)

    return permutation


def nonconvex_clusters():

    data = pd.read_csv('src/data.tsv', header=0, sep='\t')
    eps = np.arange(0.05, 0.2, 0.05)
    scores = []
    n_clusters = []
    n_outliers = []

    for e in eps:
        model = DBSCAN(e)
        model.fit(data.loc[:, ['X1', 'X2']])
        nc = len(np.unique([l for l in model.labels_ if l != -1]))
        n_clusters.append(nc)
        if nc == 2:
            permutation = find_permutation(2, data.y, model.labels_)
            y_true = [i for i, j in zip(data.y, model.labels_) if j != -1]
            y_test = [j for i, j in zip(data.y, model.labels_) if j != -1]
            scores.append(accuracy_score(y_true, y_test))
            n_outliers.append(sum(model.labels_ == -1))
        else:
            scores.append(np.nan)
            n_outliers.append(sum(model.labels_ == -1))        
    
    return pd.DataFrame({'eps': eps, 'Score': scores, 'Clusters': n_clusters, 'Outliers': n_outliers}, dtype=float)

def main():

    print(nonconvex_clusters())

if __name__ == "__main__":
    main()
