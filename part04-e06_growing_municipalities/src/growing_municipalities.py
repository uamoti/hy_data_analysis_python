#!/usr/bin/env python3

import pandas as pd

def growing_municipalities(df):

    total = df.shape[0]
    growing = df[df['Population change from the previous year, %'] > 0].shape[0]

    return growing / total

def main():

    df = pd.read_csv('src/municipal.tsv', sep='\t', header=0, index_col=0)
    df = df['Akaa':'Äänekoski']
    grow = growing_municipalities(df) * 100
    print(f'Proportion of growing municipalities: {grow:.1f}%')

if __name__ == "__main__":
    main()
