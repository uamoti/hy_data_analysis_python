#!/usr/bin/env python3

import pandas as pd
from sklearn.linear_model import LinearRegression
import numpy as np

def mystery_data():

    data = np.loadtxt('src/mystery_data.tsv', delimiter='\t', skiprows=1)
    model = LinearRegression(fit_intercept=False)
    model.fit(data[:, :5], data[:, 5])
    
    return model.coef_

def main():

    coefficients = mystery_data()
    # print the coefficients here
    for i, coef in enumerate(coefficients, start=1):
        print(f'Coefficient of X{i} is {c}')
        
    
if __name__ == "__main__":
    main()
