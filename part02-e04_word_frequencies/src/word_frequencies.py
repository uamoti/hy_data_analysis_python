#!/usr/bin/env python3

def word_frequencies(filename='src/alice.txt'):

    count = {}
    punctuation = '!"#$%&\'()*,-./:;?@[]_'

    with open(filename) as f:
        for line in f:
            words = line.strip().split()
            for w in words:
                w = w.strip(punctuation)
                if w in count:
                    count[w] += 1
                else:
                    count[w] = 1

    return count

def main():

    word_frequencies()

if __name__ == "__main__":
    main()
