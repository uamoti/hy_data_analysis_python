#!/usr/bin/env python3

def find_matching(l, s):

    indices = []

    for i, w in enumerate(l):
        if s in w:
            indices.append(i)
            
    return indices

def main():
    pass

if __name__ == "__main__":
    main()
