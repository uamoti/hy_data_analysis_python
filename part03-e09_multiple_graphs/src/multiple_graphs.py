#!/usr/bin/env python3

import matplotlib.pyplot as plt

def main():

    x0 = [2, 4, 6, 7]
    y0 = [4, 3, 5, 1]
    x1 = [1, 2, 3, 4]
    y1 = [4, 2, 3, 1]

    plt.plot(x0, y0, 'r')
    plt.plot(x1, y1, 'b')
    plt.title('Multiple plots')
    plt.xlabel('x axis')
    plt.ylabel('y axis')
    plt.show()

if __name__ == "__main__":
    main()
