#!/usr/bin/env python3

import gzip
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split


def spam_detection(random_state=0, fraction=1.0):

    ham = gzip.open('src/ham.txt.gz').readlines()
    spam = gzip.open('src/spam.txt.gz').readlines()
    ham_frac = ham[:round(len(ham) * fraction)]
    spam_frac = spam[:round(len(spam) * fraction)]
    y = np.vstack((np.zeros((len(ham_frac), 1)), np.ones((len(spam_frac), 1))))
    X = CountVectorizer().fit_transform(ham_frac + spam_frac)
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=random_state)
    model = MultinomialNB().fit(X_train, y_train)
    predictions = model.predict(X_test)
    acc = accuracy_score(y_test, predictions)
    
    return acc, X_test.shape[0], round(y_test.shape[0] * (1 - acc))

def main():

    accuracy, total, misclassified = spam_detection()
    print("Accuracy score:", accuracy)
    print(f"{misclassified} messages miclassified out of {total}")

if __name__ == "__main__":
    main()
