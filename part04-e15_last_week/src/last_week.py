#!/usr/bin/env python3

import pandas as pd
import numpy as np

def last_week():

    df = pd.read_csv('src/UK-top40-1964-1-2.tsv', sep='\t', header=0, na_values=['New', 'Re'])
    missing_lw = [i for i in range(1, 41) if i not in df.LW.values]
    missing_lw_index = df.loc[df.LW.isna(), :].index
    missing_rows = pd.DataFrame(
        {'Pos': np.nan,
        'LW': missing_lw,
        'Title': np.nan,
        'Artist': np.nan,
        'Publisher': np.nan,
        'Peak Pos': np.nan,
        'WoC': np.nan},
        index=missing_lw_index
    )
    ppos_mask = (df['Peak Pos'] == df.Pos) & (df['Peak Pos'] != df.LW)
    df.dropna(inplace=True)
    df = pd.concat([df, missing_rows]) # add missing rows
    df.sort_values('LW', inplace=True)
    df.loc[ppos_mask, 'Peak Pos'] = np.nan
    df.loc[:, 'Pos'] = df.LW
    df.loc[:, 'LW'] = np.nan
    df.WoC -= 1
    
    return df

def main():
    df = last_week()
    print("Shape: {}, {}".format(*df.shape))
    print("dtypes:", df.dtypes)
    print(df)


if __name__ == "__main__":
    main()
