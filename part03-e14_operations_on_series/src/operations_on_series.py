#!/usr/bin/env python3

import pandas as pd

def create_series(l0, l1):

    idx = pd.Index(['a', 'b', 'c'])
    s0 = pd.Series(l0, index=idx)
    s1 = pd.Series(l1, index=idx)

    return s0, s1

    
def modify_series(s0, s1):

    val = s1['b']
    s0['d'] = val
    del s1['b']

    return s0, s1

    
def main():

    x0 = [0, 1, 2]
    x1 = [3, 4, 5]
    s0, s1 = create_series(x0, x1)
    s2, s3 = modify_series(s0, s1)

    return s2 + s3        

if __name__ == "__main__":
    main()
