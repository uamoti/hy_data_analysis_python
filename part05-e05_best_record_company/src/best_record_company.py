#!/usr/bin/env python3

import pandas as pd

def best_record_company():

    top40 = pd.read_csv('src/UK-top40-1964-1-2.tsv', sep='\t')
    best = top40.groupby('Publisher')['WoC'].sum().sort_values(ascending=False).index[0]
    
    return top40[top40.Publisher == best]

def main():

    best_record_company()
    

if __name__ == "__main__":
    main()
