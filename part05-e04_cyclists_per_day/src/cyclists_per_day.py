#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt


def split_date_continues():

    week = {
        'ma': 'Mon',
        'ti': 'Tue',
        'ke': 'Wed',
        'to': 'Thu',
        'pe': 'Fri',
        'la': 'Sat',
        'su': 'Sun'
    }

    month = {
        'tammi': 1,
        'helmi': 2,
        'maalis': 3,
        'huhti': 4,
        'touko': 5,
        'kesä': 6,
        'heinä': 7,
        'elo': 8,
        'syys': 9,
        'loka': 10,
        'marras': 11,
        'joulu': 12
    }

    def hour(val):

        if val == '00':
            return 0
        elif val[0] == '0':
            return val[1]
        else:
            return val


    df = pd.read_csv('src/Helsingin_pyorailijamaarat.csv', sep=';', header=0)
    df.dropna(how='all', inplace=True)
    records = df.Päivämäärä.apply(str.split)
    df.drop(['Päivämäärä', 'Unnamed: 21'], 1, inplace=True)
    times = pd.DataFrame.from_records(records)
    times.columns = ['Weekday', 'Day', 'Month', 'Year', 'Hour']
    times.Weekday = times.Weekday.map(week)
    week_category = pd.CategoricalDtype(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'], ordered=True)
    times.Weekday = times.Weekday.astype(week_category)
    times.Month = times.Month.map(month)
    times.Day = times.Day.astype(int)
    times.Year = times.Year.astype(int)
    times.Hour = times.Hour.apply(lambda x: x[:2])
    times.Hour = times.Hour.apply(int)

    return pd.concat([times, df], axis=1)

def cyclists_per_day():

    bikes = split_date_continues()

    return bikes.groupby(['Year', 'Month', 'Day']).sum().drop('Hour', axis=1)
    
def main():

    bikes_per_day = cyclists_per_day()
    bikes_per_day.loc[2017, 8].plot()
    plt.xticks(range(1, 32))
    plt.show()

if __name__ == "__main__":
    main()
