#!/usr/bin/env python3

import re

def extract_numbers(s):

    pattern = r'[+-]?\d+\.?\d*'

    return [float(x) for x in re.findall(pattern, s)]

def main():
    print(extract_numbers("abd 123 1.2 test 13.2 -1"))

if __name__ == "__main__":
    main()
