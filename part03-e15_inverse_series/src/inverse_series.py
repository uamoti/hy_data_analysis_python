#!/usr/bin/env python3

import pandas as pd

def inverse_series(s):

    return pd.Series(s.index, index=s.values)

def main():

    s = pd.Series(list('abcde'), index=range(5))
    inverse_series(s)

if __name__ == "__main__":
    main()
