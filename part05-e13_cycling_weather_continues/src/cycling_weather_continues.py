#!/usr/bin/env python3

import pandas as pd
from sklearn.linear_model import LinearRegression


def split_date_continues():

    week = {
        'ma': 'Mon',
        'ti': 'Tue',
        'ke': 'Wed',
        'to': 'Thu',
        'pe': 'Fri',
        'la': 'Sat',
        'su': 'Sun'
    }

    month = {
        'tammi': 1,
        'helmi': 2,
        'maalis': 3,
        'huhti': 4,
        'touko': 5,
        'kesä': 6,
        'heinä': 7,
        'elo': 8,
        'syys': 9,
        'loka': 10,
        'marras': 11,
        'joulu': 12
    }

    def hour(val):

        if val == '00':
            return 0
        elif val[0] == '0':
            return val[1]
        else:
            return val

    df = pd.read_csv('src/Helsingin_pyorailijamaarat.csv', sep=';', header=0)
    df.dropna(how='all', inplace=True)
    records = df.Päivämäärä.apply(str.split)
    df.drop(['Päivämäärä', 'Unnamed: 21'], 1, inplace=True)
    times = pd.DataFrame.from_records(records)
    times.columns = ['Weekday', 'Day', 'Month', 'Year', 'Hour']
    times.Weekday = times.Weekday.map(week)
    times.Month = times.Month.map(month)
    times.Day = times.Day.astype(int)
    times.Year = times.Year.astype(int)
    times.Hour = times.Hour.apply(lambda x: x[:2])
    times.Hour = times.Hour.apply(int)
    merged = pd.concat([times, df], axis=1)
    merged = merged[merged.Year == 2017]
    
    return merged.drop(['Weekday', 'Hour', 'Year'], axis=1).groupby(['Month', 'Day']).sum().reset_index()

def cycling_weather():

    weather = pd.read_csv('src/kumpula-weather-2017.csv', header=0)
    merged = pd.merge(split_date_continues(), weather, left_on=['Month', 'Day'], right_on=['m', 'd'])
    
    return merged.drop(['m', 'd', 'Time', 'Time zone'], axis=1).fillna(method='ffill')


def cycling_weather_continues(station):

    model = LinearRegression(fit_intercept=True)
    data = cycling_weather()
    X = data.loc[:, ['Precipitation amount (mm)', 'Snow depth (cm)', 'Air temperature (degC)']]
    #X.fillna(0, inplace=True)
    y = data.loc[:, station]
    model.fit(X, y)
    
    return (model.coef_, model.score(X, y))
    
def main():

    st = 'Baana'
    (rain, snow, temp), score = cycling_weather_continues(st)
    print(f"Measuring station: {st}")
    print(f"Regression coefficient for variable 'precipitation': {rain:.1f}")
    print(f"Regression coefficient for variable 'snow depth': {snow:.1f}")
    print(f"Regression coefficient for variable 'temperature': {temp:.1f}")
    print(f"Score: {score:.2f}")

if __name__ == "__main__":
    main()
