#!/usr/bin/env python3

import pandas as pd
from sklearn.linear_model import LinearRegression
import numpy as np


def coefficient_of_determination():

    data = np.loadtxt('src/mystery_data.tsv', delimiter='\t', skiprows=1)
    model = LinearRegression(fit_intercept=True)
    model.fit(data[:, :5], data[:, 5])
    scores = []
    scores.append([model.score(data[:, :5], data[:, 5])][0])
    for i in range(5):
        mod = LinearRegression(fit_intercept=True)
        mod.fit(data[:, i].reshape(-1, 1), data[:, 5])
        scores.append(mod.score(data[:, i].reshape(-1, 1), data[:, 5]))
        
    return scores

def main():

    score = coefficient_of_determination()
    print(f'R2-score with feature(s) X: {score[0]}')
    for i in range(1, 6):
        print(f'R2-score with feature(s) X{i}: {score[i]}')

    
if __name__ == "__main__":

    main()
