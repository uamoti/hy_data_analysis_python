#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

def center(img):

    y = (img.shape[0] - 1) / 2
    x = (img.shape[1] - 1) / 2
   
    return y, x

def radial_distance(img):

    def pythagoras(p0, p1):

        dx = p1[0] - p0[0]
        dy = p1[1] - p0[1]

        return np.sqrt(np.sum([dx ** 2, dy ** 2]))

    
    middle = center(img)
    rdist = [
        pythagoras((i, j), middle)
        for i in range(img.shape[0])
        for j in range(img.shape[1])
    ]

    return np.array(rdist).reshape(img.shape[0], img.shape[1])


def scale(arr, tmin=0.0, tmax=1.0):

    a = arr.copy()
    if a.min() == a.max():
        return a * 0
    else:
        span = a.max() - a.min()
        return (a - a.min()) / span


def radial_mask(img):

    dist = radial_distance(img)
    sdist = scale(dist)

    return 1 - sdist


def radial_fade(img):

    mask = radial_mask(img)

    return (img.transpose() * mask.transpose()).transpose()

 
def main():

    img = plt.imread('src/painting.png')
    fig, ax = plt.subplots(3, 1)
    ax[0].imshow(img)
    ax[1].imshow(radial_mask(img))
    ax[2].imshow(radial_fade(img))
    plt.show()

if __name__ == "__main__":
    main()
