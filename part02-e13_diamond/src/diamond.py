#!/usr/bin/env python3

import numpy as np

def diamond(n):

    if n == 1:
        return np.array([1], dtype=int).reshape(1, 1)
    else:
        I = np.eye(n, dtype=int)
        M = np.concatenate([I[:, n-1:0:-1], I], axis=1)
        N = np.concatenate([M, M[n-2::-1, :]])
        return N

def main():

    print(diamond(3))
    print(diamond(1))

if __name__ == "__main__":
    main()
