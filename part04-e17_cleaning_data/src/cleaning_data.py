#!/usr/bin/env python3

import pandas as pd
import numpy as np


def cleaning_data():

    def first_last_name(name):
    
        if ',' in name:
            name = name.replace(',', '')
            last, first = name.split()
            
            return first.capitalize() + ' ' + last.capitalize()
        else:
            first, last = name.split()
            
            return first.capitalize() + ' ' + last.capitalize()

    
    dtypes = {'President': object, 'Start': int, 'Last': float, 'Seasons': int, 'Vice-president': object}
    pres = pd.read_csv('src/presidents.tsv', header=0, sep='\t')
    pres.Last = pres.Last.str.replace('-', 'nan')
    pres.Seasons = pres.Seasons.str.replace('two', '2')
    pres.Start = pres.Start.str.replace(r'\s[a-zA-Z]*', '', regex=True)
    pres.President = pres.President.apply(first_last_name)
    pres['Vice-president'] = pres['Vice-president'].apply(first_last_name)
    
    return pres.astype(dtypes)


def main():

    cleaning_data()

if __name__ == "__main__":
    main()
