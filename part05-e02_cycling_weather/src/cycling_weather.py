#!/usr/bin/env python3

import pandas as pd


def split_date_continues():

    week = {
        'ma': 'Mon',
        'ti': 'Tue',
        'ke': 'Wed',
        'to': 'Thu',
        'pe': 'Fri',
        'la': 'Sat',
        'su': 'Sun'
    }

    month = {
        'tammi': 1,
        'helmi': 2,
        'maalis': 3,
        'huhti': 4,
        'touko': 5,
        'kesä': 6,
        'heinä': 7,
        'elo': 8,
        'syys': 9,
        'loka': 10,
        'marras': 11,
        'joulu': 12
    }

    def hour(val):

        if val == '00':
            return 0
        elif val[0] == '0':
            return val[1]
        else:
            return val


    df = pd.read_csv('src/Helsingin_pyorailijamaarat.csv', sep=';', header=0)
    df.dropna(how='all', inplace=True)
    records = df.Päivämäärä.apply(str.split)
    df.drop(['Päivämäärä', 'Unnamed: 21'], 1, inplace=True)
    times = pd.DataFrame.from_records(records)
    times.columns = ['Weekday', 'Day', 'Month', 'Year', 'Hour']
    times.Weekday = times.Weekday.map(week)
    times.Month = times.Month.map(month)
    times.Day = times.Day.astype(int)
    times.Year = times.Year.astype(int)
    times.Hour = times.Hour.apply(lambda x: x[:2])
    times.Hour = times.Hour.apply(int)

    return pd.concat([times, df], axis=1)


def cycling_weather():

    weather = pd.read_csv('src/kumpula-weather-2017.csv', header=0, na_values={'Precipitation amount(mm)': -1, 'Snow depth (cm)': -1})
    merged = pd.merge(split_date_continues(), weather, left_on=['Year', 'Month', 'Day'], right_on=['Year', 'm', 'd'])
    
    return merged.drop(['m', 'd', 'Time', 'Time zone'], axis=1)

def main():

    cycling_weather()

if __name__ == "__main__":
    main()
