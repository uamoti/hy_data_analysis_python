# Enter you module contents here
"""
A simple module to calculate the hypothenuse and area of right-angled triangle.
"""

__version__ = "0.1"

__author__ = "Bernardo Moreira Soares Oliveira"


import math
 

def hypothenuse(a, b):

    """
    Returns the value of the hypothenuse of a right-angled triangle,

    given the other two sides.

    Params:

      - a: int

      - b: int

    Returns:

    - h: int - hypothenuse

    """

    return math.sqrt(a**2 + b**2)

 
def area(a, b):

    """
    Returns the area of a right-angled triangle, given two sides.

    Params:

      - a: int

      - b: int

    Returns:

    - a: int - area

    """

    return (a * b) / 2

 
