#!/usr/bin/env python3

import re


def file_listing(filename="src/listing.txt"):

    def format_output(l):

        new_line = tuple(
            [int(l[0]), l[1], int(l[2]), int(l[3]), int(l[4]), l[5]]
        )

        return new_line
    

    pattern = re.compile(
        r'(\d+)\s+([A-Z?][a-z]+)\s+(\d+)\s+(\d+):(\d+)\s+(\w*.\w+)'
    )

    lines = [l.strip() for l in open('src/listing.txt').readlines()]
    result = [pattern.findall(l)[0] for l in lines]
    result = [format_output(t) for t in result]

    return result

def main():

    file_listing()

if __name__ == "__main__":
    main()
