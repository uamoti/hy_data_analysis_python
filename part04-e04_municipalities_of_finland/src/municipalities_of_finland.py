#!/usr/bin/env python3

import pandas as pd

def municipalities_of_finland():

    df = pd.read_csv('src/municipal.tsv', sep='\t', header=0, index_col=0)
    #drop = [i for i in df.index if 'sub-region' in i]
    #drop.append('WHOLE COUNTRY')

    return df['Akaa':'Äänekoski']
    
def main():

    municipalities_of_finland()
    
if __name__ == "__main__":
    main()
