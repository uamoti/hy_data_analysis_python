#!/usr/bin/env python3

import pandas as pd

def read_series():

    items = {}

    while True:
        line = input('Enter index and value: ').split()
        if line == []:
            break
        elif len(line) != 2:
            raise ValueError('Invalid')
            break
        else:
            items[line[0]] = line[1]

    return pd.Series(items)


def main():

    read_series()

if __name__ == "__main__":
    main()
