#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
import re


def commute():

    month = {
        'tammi': '01',
        'helmi': '02',
        'maalis': '03',
        'huhti': '04',
        'touko': '05',
        'kesä': '06',
        'heinä': '07',
        'elo': '08',
        'syys': '09',
        'loka': '10',
        'marras': '11',
        'joulu': '12'
    }
    week = {
        'ma': 'Mon',
        'ti': 'Tue',
        'ke': 'Wed',
        'to': 'Thu',
        'pe': 'Fri',
        'la': 'Sat',
        'su': 'Sun'
    }
    pattern = '^([a-z][a-z]) (\d+) (\w+) ([1-2][0-9][0-9][0-9]) ([0-2][0-9]:[0-5][0-9])'
    bike = pd.read_csv('src/Helsingin_pyorailijamaarat.csv', sep=';', header=0)
    bike.drop('Unnamed: 21', axis=1, inplace=True)
    bike.dropna(subset=['Päivämäärä'], inplace=True)
    match = bike.Päivämäärä.apply(lambda x: re.match(pattern, x).groups())
    match = match.apply(lambda x: (week[x[0]], x[1], month[x[2]], x[3], x[4]))
    match = match.apply(' '.join)
    weekday = match.apply(lambda x: x[:3])
    weekday_category = pd.CategoricalDtype(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'], ordered=True)
    bike.Päivämäärä = pd.to_datetime(match, dayfirst=True)
    bike['Weekday'] = weekday
    bike.Weekday = bike.Weekday.astype(weekday_category)
    
    return bike.set_index('Päivämäärä').loc['2017-08-01': '2017-08-31'].groupby('Weekday').sum()


def main():

    df = commute()
    df.plot()
    plt.show()


if __name__ == "__main__":
    main()
