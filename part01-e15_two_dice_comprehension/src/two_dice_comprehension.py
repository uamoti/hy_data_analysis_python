#!/usr/bin/env python3

def main():

    combinations = [(i, j)
        for i in range(1, 7)
        for j in range(1, 7)
        if i + j == 5]

    for c in combinations:
        print(c)

if __name__ == "__main__":
    main()
