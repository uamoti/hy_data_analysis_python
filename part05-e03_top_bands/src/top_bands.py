#!/usr/bin/env python3

import pandas as pd

def top_bands():

    top40 = pd.read_csv('src/UK-top40-1964-1-2.tsv', header=0, sep='\t')
    bands = pd.read_csv('src/bands.tsv', header=0, sep='\t')
    top40.Artist = top40.Artist.str.title()

    return pd.merge(top40, bands, left_on='Artist', right_on='Band', how='right')

def main():

    top_bands()

if __name__ == "__main__":
    main()
