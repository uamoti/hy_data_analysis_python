#!/usr/bin/env python3

import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

def fit_line(x, y):

    model = LinearRegression(fit_intercept=True)
    model.fit(x.reshape(-1, 1), y)
    #xfit = np.linspace(x.min(), x.max(), 100)
    #yfit = model.predict(xfit.reshape(-1, 1))
    
    return model.coef_[0], model.intercept_
    
def main():

    x = np.linspace(0, 10, 20)
    y = 3 * x + 1 + 0.8 * np.random.randn(20)
    slope, intercept = fit_line(x, y)
    print(f'Slope: {slope}')
    print(f'Intercept: {intercept}')
    yfit = x * slope + intercept
    plt.plot(x, y, 'o')
    plt.plot(x, yfit, 'r')
    plt.show()

    
if __name__ == "__main__":
    main()
