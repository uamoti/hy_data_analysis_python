#!/usr/bin/env python3

import numpy as np
import functools

def matrix_power(M, n):

    if n == 0:
        return np.eye(M.shape[0])
    elif n < 0:
        return np.linalg.inv(matrix_power(M,-n))
    else:
        return functools.reduce(np.matmul, [M] * n)

def main():

    M = np.arange(16).reshape(4, 4)
    matrix_power(M, 5)

if __name__ == "__main__":
    main()
