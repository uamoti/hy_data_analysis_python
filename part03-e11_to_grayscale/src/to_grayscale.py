#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

def to_grayscale(img):

    weights = np.array([0.2126, 0.7152, 0.0722])

    return np.average(img, axis=2, weights=weights)

 
def to_red(arr):

    red_array = arr.copy()
    red_array[:, :, 1:] = 0

    return red_array

 
def to_green(arr):

    green_array = arr.copy()
    green_array[:, :, [0, 2]] = 0

    return green_array

 
def to_blue(arr):

    blue_array = arr.copy()
    blue_array[:, :, :2] = 0

    return blue_array

 
def main():

    painting = plt.imread('src/painting.png')
    plt.gray()
    plt.imshow(to_grayscale(painting))
    fig, ax = plt.subplots(3, 1)
    ax[0].imshow(to_red(painting))
    ax[1].imshow(to_green(painting))
    ax[2].imshow(to_blue(painting))
    plt.show()

if __name__ == "__main__":
    main()
