#!/usr/bin/env python3

def triple(x):
    return x * 3

def square(x):
    return x ** 2



def main():

    for i in range(1, 11):
        tri = triple(i)
        sqr = square(i)
        if sqr > tri:
            break
        else:
            print(f"triple({i})=={tri}", f"square({i})=={sqr}")

if __name__ == "__main__":
    main()
