#!/usr/bin/env python3

import numpy as np
import scipy.linalg

def vector_angles(X, Y):

    dot_prods = np.sum(X * Y, axis=1)
    mag_prods = np.sqrt(np.sum(X**2, axis=1)) * np.sqrt(np.sum(Y**2, axis=1))
    angle_cosines = dot_prods / mag_prods
    angles = np.degrees(np.arccos(np.clip(angle_cosines, -1.0, 1.0)))

    return angles

def main():

    M = np.random.random_sample(100).reshape(10, 10)
    N = np.random.random_sample(100).reshape(10, 10)
    vector_angles(M, N)

if __name__ == "__main__":
    main()
