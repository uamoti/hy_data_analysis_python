#!/usr/bin/env python3

import pandas as pd
import numpy as np

data = {
    'State': ['United Kingdom', 'Finland', 'USA', 'Sweden', 'Germany', 'Russia'],
    'Year of independence': [np.nan, 1917, 1776, 1523, np.nan, 1992],
    'President': [np.nan, 'Niinistö', 'Trump', np.nan, 'Steinmeier', 'Putin']
}

def missing_value_types():

    df = pd.DataFrame(data)

    return df.set_index('State')

def main():

    missing_value_types()

if __name__ == "__main__":
    main()
