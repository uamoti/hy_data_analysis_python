#!/usr/bin/env python3

from sklearn.decomposition import PCA
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def explained_variance():

    data = pd.read_csv('src/data.tsv', header=0, sep='\t')
    pca = PCA(10)
    pca.fit(data)
    #evar = [round(ev, 3) for ev in pca.explained_variance_]
    
    return data.var(), pca.explained_variance_

def main():
    v, ev = explained_variance()
    print(sum(v), sum(ev))

    print('The variances are:', end='')
    for var in v:
        print(f'{var:.3f}', end=' ')

    print('\nThe explained variances after PCA are:', end=' ')
    for evar in ev:
        print(f'{evar:.3f}', end=' ')

    plt.plot(range(1, len(ev) + 1), ev.cumsum())
    plt.show()

if __name__ == "__main__":
    main()
