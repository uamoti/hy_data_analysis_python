#!/usr/bin/env python3

def detect_ranges(arr):

    ranges = []
    x = sorted(arr)

    i = 0

    while i < len(x):
        start = x[i]
        if start + 1 not in x:
            ranges.append(start)
            i += 1
        else:
            end = x[i]
            while end + 1 in x:
                i += 1
                end = x[i]
            ranges.append((start, end + 1))
            i += 1
            
    return ranges

def main():
    L = [2, 5, 4, 8, 12, 6, 7, 10, 13]
    result = detect_ranges(L)
    print(L)
    print(result)

if __name__ == "__main__":
    main()
