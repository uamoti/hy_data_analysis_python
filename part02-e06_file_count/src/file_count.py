#!/usr/bin/env python3

import sys

def file_count(filename):

    lines, words, chars = 0, 0, 0

    for l in open(filename):
        lines += 1
        words += len(l.split())
        chars += len(l)

    return (lines, words, chars)


def main():

    for fname in sys.argv[1:]:
        l, w, c = file_count(fname)
        print(f"{l}\t{w}\t{c}\t{fname}")

if __name__ == "__main__":
    main()
