#!/usr/bin/env python3

import math

def solve_quadratic(a, b, c):

    d = math.sqrt(b**2 - 4 * a * c)
    x0 = (-b + d) / (2 * a)
    x1 = (-b - d) / (2 * a)

    return (x0,x1)


def main():
    print(solve_quadratic(1, -3, 2))
    print(solve_quadratic(1, 2, 1))
    print(solve_quadratic(2.250365, 4.923733, 1.375028))
    print(solve_quadratic(-2, 2, 1))

if __name__ == "__main__":
    main()
