#!/usr/bin/env python3

import math

def triangle_area():

    b = float(input('Give the base of the triangle: '))
    h = float(input('Give the height of the triangle: '))

    return 0.5 * b * h

def rectangle_area():

    w = float(input('Give the width of the rectangle: '))
    h = float(input('Give the height of the rectangle: '))

    return w * h

def circle_area():

    r = float(input('Give the radius of the circle: '))

    return r ** 2 * math.pi


def main():
    # enter you solution here
    shapes = ['triangle', 'rectangle', 'circle']
    chosen = None

    while True:
        chosen = input('Choose a shape [triangle, rectangle, circle]: ')
        if chosen == '':
            break
        elif chosen == 'triangle':
            print(f'The area is {triangle_area():.6f}')
        elif chosen == 'rectangle':
            print(f'The area is {rectangle_area():.6f}')
        elif chosen == 'circle':
            print(f'The area is {circle_area():.6f}')
        else:
            print('Unknown shape!')

if __name__ == "__main__":
    main()
