#!/usr/bin/env python3

def merge(L1, L2):

    merged = []
    i, j = 0, 0

    while i < len(L1) and j < len(L2):
        if L1[i] < L2[j]:
            merged.append(L1[i])
            i += 1
        else:
            merged.append(L2[j])
            j += 1

    if i >= len(L1):
        merged += L2[j:]
    elif j >= len(L2):
        merged += L1[i:]

    return merged


def main():
    print(merge([1, 2, 7, 8], [3, 5, 9]))
    print(merge([-2, 3, -4, 9], [0, -1, 5, 2]))

if __name__ == "__main__":
    main()
